package sample.util;

import org.json.JSONArray;
import org.json.JSONObject;
import sample.models.Question;
import sample.models.Quiz;
import sample.models.Student;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class DataCollector {

    public static void main(String[] args) throws Exception{
        Quiz.createTable();
        Question.createTable();
        Student.createTable();

//        readAndSaveQuizzesData();
        readAndSaveUserData();
    }


    public static void readAndSaveUserData() throws Exception{

        File file = new File( "src/sample/util/sample_data/users.json" );

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        StringBuilder stringBuilder = new StringBuilder();
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }

        //parsing json
        JSONArray result = new JSONArray(stringBuilder.toString());

        for (int i = 0; i < result.length(); i++) {

            String objString = result.get(i).toString();
            JSONObject obj = new JSONObject(objString);

            Student student = new Student();

            student.setFirstName(obj.getString("firstName"));
            student.setLastName(obj.getString("lastName"));
            student.setEmail(obj.getString("email"));
            student.setPassword(obj.getInt("password") + "");
            student.setMobile(obj.getInt("phone") + "");
            student.setGender('M');

            student.save();


        }
    }




    public static void readAndSaveQuizzesData() throws Exception{

        String folderPath = "src/sample/util/sample_data/quizzes";

        File folder = new File(folderPath);
        String[] fileNames = folder.list();

        for(String fileName : fileNames) {
            System.out.println(fileName);

            File file = new File( folderPath + "/" + fileName );
//        System.out.println(file.exists());
//        System.out.println(file.getAbsolutePath());

            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
//        System.out.println(stringBuilder);

            JSONObject jsonObject = new JSONObject(stringBuilder.toString());
            JSONArray result = jsonObject.getJSONArray("results");
            Quiz quiz = new Quiz();
            List<Question> questions = new ArrayList<>();

            for (int i = 0; i < result.length(); i++) {

                String objString = result.get(i).toString();
                JSONObject obj = new JSONObject(objString);
                Question question = new Question();
                question.setQuestion(obj.getString("question"));

                JSONArray incorrectOptions = obj.getJSONArray("incorrect_answers");

                quiz.setTitle(obj.getString("category"));
                question.setOption1(incorrectOptions.get(0).toString());
                question.setOption2(incorrectOptions.get(1).toString());
                question.setOption3(incorrectOptions.get(2).toString());
                question.setOption4(obj.getString("correct_answer"));
                question.setAnswer(obj.getString("correct_answer"));
                question.setQuiz(quiz);

                questions.add(question);

                System.out.println(question);
                System.out.println(quiz);

            }

            quiz.save(questions);
        }

    }

}