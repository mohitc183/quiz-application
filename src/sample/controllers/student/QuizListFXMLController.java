package sample.controllers.student;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.FlowPane;
import sample.listeners.NewScreenListener;
import sample.models.Quiz;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

public class QuizListFXMLController implements Initializable {

    @FXML
    private FlowPane quizListContainer;


    Map<Quiz, Integer> quizzes = null;
    private Set<Quiz> keys;

    private NewScreenListener screenListener;


    public void setScreenListener(NewScreenListener screenListener) {
        this.screenListener = screenListener;
        setCards();
    }

    private void setCards(){
        for(Quiz q : keys){
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/fxml/student/QuizCardLayoutFXML.fxml"));

            try {
                Node node = fxmlLoader.load();
                QuizCardLayoutFXMLController  quizCardLayoutFXMLController = fxmlLoader.getController();
                quizCardLayoutFXMLController.setQuiz(q);
//                quizCardLayoutFXMLController.setTitle(q.getTitle());
                quizCardLayoutFXMLController.setNoq(quizzes.get(q) + "");
                quizCardLayoutFXMLController.setScreenListener(this.screenListener);
                quizListContainer.getChildren().add(node);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        quizzes = Quiz.getAllWithQuestionCount();
        keys = quizzes.keySet();

    }
}
