package sample.controllers.student;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import javafx.application.Platform;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableStringValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.FlowPane;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import sample.models.Question;
import sample.models.Quiz;
import sample.models.QuizResult;
import sample.models.Student;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class QuestionsScreenFXMLController implements Initializable {

    @FXML private FlowPane progressPane;

    private class QuestionsObservable{
        Property<String> question = new SimpleStringProperty();
        Property<String> option1 = new SimpleStringProperty();
        Property<String> option2 = new SimpleStringProperty();
        Property<String> option3 = new SimpleStringProperty();
        Property<String> option4 = new SimpleStringProperty();
        Property<String> answer = new SimpleStringProperty();



        public void setQuestion(Question question) {
            this.question.setValue(question.getQuestion());
            this.option1.setValue(question.getOption1());
            this.option2.setValue(question.getOption2());
            this.option3.setValue(question.getOption3());
            this.option4.setValue(question.getOption4());
            this.answer.setValue(question.getAnswer());
        }
    }

    private QuestionsObservable questionsObservable;

    @FXML
    private Label title;

    @FXML private Label time;

    @FXML private Label question;

    @FXML private JFXRadioButton option1;

    @FXML private JFXRadioButton option2;

    @FXML private JFXRadioButton option3;

    @FXML private JFXRadioButton option4;

    @FXML private ToggleGroup options;

    @FXML private JFXButton next;

    @FXML private JFXButton submit;

    private Quiz quiz;
    private List<Question> questionsList;
    private Question currentQuestion;
    int currentIndex = 0;

    static long min, sec, hr, totalSecs = 0;

    private Map<Question, String> studentAnswers = new HashMap<>();

    private Integer numberOfRightAnswers = 0;

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
        this.title.setText(this.quiz.getTitle());
        this.getData();
    }


    public void convertTime(){
        min = TimeUnit.SECONDS.toMinutes(totalSecs);
        sec = totalSecs - (min * 60);
        hr = TimeUnit.MINUTES.toHours(min);
        min = min - (hr * 60);

        time.setText( format(hr) + ":" + format(min) + ":" + format(sec) );

        totalSecs--;
    }

    private static String format(long value){
        if(value < 10){
            return 0 + "" + value;
        }
        else {
            return value + "";
        }
    }

    private void setTimer(){

        Timer timer = new Timer();
        totalSecs = this.questionsList.size() * 2;

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {

                        convertTime();
                        if(totalSecs <=0 ){
                            timer.cancel();
                            time.setText("00:00:00");
                            Notifications.create()
                                    .darkStyle()
                                    .title("Time UP!!")
                                    .text("There are no more questions")
                                    .position(Pos.BOTTOM_LEFT)
                                    .hideAfter(Duration.millis(2000))
                                    .showInformation();
                        }
                    }
                });
            }
        };

        timer.schedule(timerTask, 0 ,1000);
    }

    private void getData(){
        if (quiz != null){
            this.questionsList = quiz.getQuestions();
            Collections.shuffle(this.questionsList);
            renderProgress();
            setNextQuestion();
            setTimer();
        }
    }

    private void renderProgress(){


        for(int i=0; i < this.questionsList.size(); i++){
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/fxml/student/ProgressCircleFXML" +
                    ".fxml"));
            try {
                Node node = fxmlLoader.load();
                ProgressCircleFXMLController progressCircleFXMLController = fxmlLoader.getController();
                progressCircleFXMLController.setNumber(i+1);
                progressCircleFXMLController.setDefaultColor();
                progressPane.getChildren().add(node);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.showNextQuestionButton();
        this.hideSubmitQuizButton();

        this.questionsObservable = new QuestionsObservable();
        bindFields();

        this.option1.setSelected(true);

    }

    private void bindFields(){
        this.question.textProperty().bind(this.questionsObservable.question);
        this.option4.textProperty().bind(this.questionsObservable.option4);
        this.option3.textProperty().bind(this.questionsObservable.option3);
        this.option2.textProperty().bind(this.questionsObservable.option2);
        this.option1.textProperty().bind(this.questionsObservable.option1);

    }

    private void setNextQuestion(){
        if( !(currentIndex >= questionsList.size() ) ){

            {
                //Chnaging the color
                Node circleNode = this.progressPane.getChildren().get(currentIndex);
                ProgressCircleFXMLController controller = (ProgressCircleFXMLController) circleNode.getUserData();
                controller.setCurrentQuestionColor();
            }

            this.currentQuestion = this.questionsList.get(currentIndex);

            List<String> options = new ArrayList<>();
            options.add(this.currentQuestion.getOption1());
            options.add(this.currentQuestion.getOption2());
            options.add(this.currentQuestion.getOption3());
            options.add(this.currentQuestion.getOption4());

            Collections.shuffle(options);

            this.currentQuestion.setOption1(options.get(0));
            this.currentQuestion.setOption2(options.get(1));
            this.currentQuestion.setOption3(options.get(2));
            this.currentQuestion.setOption4(options.get(3));

//            this.question.setText(this.currentQuestion.getQuestion());
//            this.option1.setText(options.get(0));
//            this.option2.setText(options.get(1));
//            this.option3.setText(options.get(2));
//            this.option4.setText(options.get(3));

            this.questionsObservable.setQuestion(this.currentQuestion);
            currentIndex++;

        }else {
            Notifications.create()
                    .darkStyle()
                    .title("No Question")
                    .text("There are no more questions")
                    .position(Pos.BOTTOM_LEFT)
                    .hideAfter(Duration.millis(2000))
                    .showInformation();

            hideNextQuestionButton();
            showSubmitQuizButton();
        }
    }

    private void hideNextQuestionButton(){
        this.next.setVisible(false);
    }

    private void showNextQuestionButton(){
        this.next.setVisible(true);
    }

    private void hideSubmitQuizButton(){
        this.submit.setVisible(false);
    }

    private void showSubmitQuizButton(){
        this.submit.setVisible(true);
    }

    public void nextQuestion(ActionEvent event) {

        boolean isRight = false;
        {
            JFXRadioButton selected = (JFXRadioButton) options.getSelectedToggle();
            String userAnswer = selected.getText();
            String rightAnswer = this.currentQuestion.getAnswer();

            if(userAnswer.trim().equalsIgnoreCase(rightAnswer.trim())){
                isRight = true;
                this.numberOfRightAnswers++;

            }
            //saving ans to hashmap
            studentAnswers.put(this.currentQuestion, userAnswer.trim());
        }
        Node circleNode = this.progressPane.getChildren().get(currentIndex-1);
        ProgressCircleFXMLController controller = (ProgressCircleFXMLController) circleNode.getUserData();
        if(isRight){

            controller.setRightAnsweredColor();

        }else {

            controller.setWrongAnsweredColor();
        }
        this.setNextQuestion();
    }

    public void submit(ActionEvent event) {
        System.out.println(this.studentAnswers);
        Student student = new Student();
        student.setId(1);
        QuizResult quizResult = new QuizResult(this.quiz, student, numberOfRightAnswers);
        boolean result = quizResult.save(this.studentAnswers);

        if(result){
            Notifications.create()
                    .darkStyle()
                    .hideAfter(Duration.millis(2000))
                    .text("Attempted!!")
                    .title("Quiz")
                    .position(Pos.CENTER)
                    .showInformation();

        }else {
            Notifications.create()
                    .darkStyle()
                    .hideAfter(Duration.millis(2000))
                    .text("Something went wrong")
                    .title("Error!")
                    .position(Pos.BASELINE_RIGHT)
                    .showError();
        }
    }


}
