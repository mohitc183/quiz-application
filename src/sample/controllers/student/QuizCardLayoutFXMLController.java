package sample.controllers.student;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import sample.listeners.NewScreenListener;
import sample.models.Quiz;

import java.net.URL;
import java.util.ResourceBundle;

public class QuizCardLayoutFXMLController  implements Initializable {

    @FXML
    private Label title;

    @FXML
    private Label noq;

    @FXML
    private JFXButton startButton;

    private Quiz quiz;

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
        this.title.setText(this.quiz.getTitle());
    }

    private NewScreenListener screenListener;

    public void setScreenListener(NewScreenListener screenListener) {
        this.screenListener = screenListener;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setNoq(String value) {
        this.noq.setText(value);
    }



    public void startQuiz(ActionEvent event) {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/fxml/student/QuestionsScreenFXML.fxml"));

        try{
            Node node = fxmlLoader.load();
            QuestionsScreenFXMLController questionsScreenFXMLController = fxmlLoader.getController();
            questionsScreenFXMLController.setQuiz(this.quiz);

            this.screenListener.changeScreen(node);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
