package sample.controllers.student;

import com.jfoenix.controls.JFXButton;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import sample.listeners.NewScreenListener;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StudentMainScreenController implements Initializable {

    @FXML
    private JFXButton backButton;

    @FXML
    private StackPane stackPanel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        addQuizListScreen();
    }

    private void addScreenToStackPane(Node node){
        this.stackPanel.getChildren().add(node);
    }

    private void addQuizListScreen(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sample/fxml/student/QuizListFXML.fxml"));

        try {
            Node node = fxmlLoader.load();
            QuizListFXMLController quizListFXMLController = fxmlLoader.getController();
            quizListFXMLController.setScreenListener(new NewScreenListener() {
                @Override
                public void changeScreen(Node node) {
                    addScreenToStackPane(node);
                }

                @Override
                public void handle(Event event) {

                }
            });
            stackPanel.getChildren().add(node);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void back(ActionEvent event) {
        ObservableList<Node> nodes =this.stackPanel.getChildren();

        if(nodes.size() == 1){
            return;
        }
        this.stackPanel.getChildren().remove(nodes .size()-1);
    }
}
