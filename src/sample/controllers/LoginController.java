package sample.controllers;

import javafx.geometry.Pos;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import sample.constants.AdminEmailPassword;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import java.net.URL;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import sample.exceptions.LoginException;
import sample.models.Student;

import java.sql.SQLException;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    @FXML
    private TextField adminEmail;

    @FXML
    private PasswordField adminPassword;

    @FXML
    private TextField studentEmail;

    @FXML
    private PasswordField studentPassword;

    @FXML
    private Button studentLoginButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void loginAdmin(ActionEvent actionEvent) {
        String email = adminEmail.getText();
        String password = adminPassword.getText();

        if(email.trim().equalsIgnoreCase(AdminEmailPassword.email) && password.trim().equalsIgnoreCase(AdminEmailPassword.password)){

            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/sample/fxml/AdminHomeScreenFXML.fxml"));
                Stage stage = (Stage)studentPassword.getScene().getWindow();
                Scene scene = new Scene(root);
                stage.setScene(scene);
//                stage.setFullScreen(true);
                stage.setMaximized(true);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                System.exit(0);
            }


            System.out.println("Admin Login Success!!!");
        }else{
            System.out.println("Admin Login Failed!!!");
        }

//        System.out.println("Login Admin called");
    }

    @FXML
    public void loginStudent(ActionEvent actionEvent) {

        System.out.println("Login Student called");

        Student s = new Student(this.studentEmail.getText(), this.studentPassword.getText());

        try {
            //s.login();
            System.out.println(s);

            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/sample/fxml/student/StudentMainScreen.fxml"));
                Stage stage = (Stage)studentPassword.getScene().getWindow();
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.setFullScreen(true);
//                stage.setMaximized(true);
            } catch (Exception e) {
               e.printStackTrace();
            }

        } catch (Exception e) {
            if (e instanceof LoginException){
                Notifications.create()
                        .title("Login Failed")
                        .text("Email Or Password Incorrect!!")
                        .position(Pos.CENTER)
                        .darkStyle()
                        .hideAfter(Duration.millis(2000))
                        .showError();
            }
        }
    }
}
