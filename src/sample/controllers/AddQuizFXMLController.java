package sample.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTreeView;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;

import javafx.event.ActionEvent;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import sample.models.Question;
import sample.models.Quiz;

import javax.management.Notification;
import java.net.URL;
import java.util.*;

public class AddQuizFXMLController implements Initializable {


    @FXML
    public TextField quizTitle;

    @FXML
    public TextArea question;

    @FXML
    public TextField option1;

    @FXML
    public TextField option2;

    @FXML
    public TextField option3;

    @FXML
    public TextField option4;

    @FXML
    public JFXRadioButton option1radio;

    @FXML
    public JFXRadioButton option2radio;

    @FXML
    public JFXRadioButton option3radio;

    @FXML
    public JFXRadioButton option4radio;

    @FXML
    public JFXButton addNextQuestion;

    @FXML
    public JFXButton submitQuiz;

    @FXML
    public JFXButton setQuizTitleButton;

    @FXML
    private JFXTreeView treeView;

    private ToggleGroup radioGroup;

    //my variables
    private Quiz quiz = null;

    private ArrayList<Question> questions = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        radioButtonSetup();
        renderTreView();

    }

    private void renderTreView(){
        Map<Quiz, List<Question>> data = Quiz.getAll();
        Set<Quiz> quizzes =  data.keySet();

        TreeItem root = new TreeItem("quizzes");
        for (Quiz q : quizzes){

            TreeItem quizTreeItem = new TreeItem(q);

            List<Question> questions = data.get(q);
            for (Question question : questions){

                TreeItem questionTreeItem = new TreeItem(question);
                questionTreeItem.getChildren().add(new TreeItem<>("A :" + question.getOption1()));
                questionTreeItem.getChildren().add(new TreeItem<>("B :" + question.getOption2()));
                questionTreeItem.getChildren().add(new TreeItem<>("C :" + question.getOption3()));
                questionTreeItem.getChildren().add(new TreeItem<>("D :" + question.getOption4()));
                questionTreeItem.getChildren().add(new TreeItem<>("Ans :" + question.getAnswer()));
                quizTreeItem.getChildren().add(questionTreeItem);
            }
            quizTreeItem.setExpanded(true);
            root.getChildren().add(quizTreeItem);
        }

        this.treeView.setRoot(root);

        root.setExpanded(true);
    }

    private void radioButtonSetup(){

        radioGroup = new ToggleGroup();
        option1radio.setToggleGroup(radioGroup);
        option2radio.setToggleGroup(radioGroup);
        option3radio.setToggleGroup(radioGroup);
        option4radio.setToggleGroup(radioGroup);

    }

    @FXML
    public void setQuizTitle(ActionEvent actionEvent) {

        String title = quizTitle.getText();
        if(title.trim().isEmpty()){

            Notifications.create()
                    .darkStyle()
                    .hideAfter(Duration.millis(2000))
                    .text("Enter A Valid Title !!!")
                    .title("Invalid Quiz Title")
                    .position(Pos.CENTER)
                    .showError();
        }else{
            System.out.println("Title Added.....");
            quizTitle.setEditable(false);
            this.quiz = new Quiz(title);
        }
    }

    private boolean validateFields(){

        if(quiz == null){
            Notifications.create()
                    .title("Quiz")
                    .darkStyle()
                    .position(Pos.CENTER)
                    .text("Please Enter Quiz Title!!")
                    .showError();
            return false;
        }

        String question = this.question.getText();
        String op1 = this.option1.getText();
        String op2 = this.option2.getText();
        String op3 = this.option3.getText();
        String op4 = this.option4.getText();
        Toggle selectedRadio = radioGroup.getSelectedToggle();

        if(question.trim().isEmpty() ||
                op1.trim().isEmpty() ||
                op2.trim().isEmpty() ||
                op2.trim().isEmpty() ||
                op3.trim().isEmpty() ||
                op4.trim().isEmpty()    ){

            Notifications.create()
                    .title("Question")
                    .darkStyle()
                    .position(Pos.CENTER)
                    .text("All fields are Required... \nQuestion, Option1, Option2, Option3, Option4")
                    .showError();
            return false;
        }else{

            if(selectedRadio == null){
                Notifications.create()
                        .title("Question")
                        .darkStyle()
                        .position(Pos.CENTER)
                        .text("Select An Answer!!!")
                        .showError();
                return false;
            }else{
                return true;
            }
        }
    }

    @FXML
    private void addNextQuestion(ActionEvent event){

        addQuestions();
    }

    private boolean addQuestions(){

        boolean valid = validateFields();
        Question question = new Question();
        if( valid ){
            //Save n add next ques
            question.setOption1(option1.getText().trim());
            question.setOption2(option2.getText().trim());
            question.setOption3(option3.getText().trim());
            question.setOption4(option4.getText().trim());

            Toggle selected = radioGroup.getSelectedToggle();
            String ans = null;

            if(selected == option1radio){

                ans = option1.getText().trim();
            }else if(selected == option2radio){

                ans = option2.getText().trim();
            }else if(selected == option3radio){

                ans = option3.getText().trim();
            }if(selected == option4radio){

                ans = option4.getText().trim();
            }

            question.setAnswer(ans);
            question.setQuestion(this.question.getText());


            this.question.clear();
            option1.clear();
            option2.clear();
            option3.clear();
            option4.clear();
            selected.setSelected(false);

            questions.add(question);


            question.setQuiz(quiz);


            System.out.println(questions);
            System.out.println(quiz);
        }
        return valid;
    }

    @FXML
    private void submitQuiz(ActionEvent event){

        if (addQuestions()){
            if ( quiz.save(questions ) ){
                Notifications.create()
                        .title("Success")
                        .darkStyle()
                        .position(Pos.CENTER)
                        .hideAfter(Duration.millis(2000))
                        .text("Quiz Added Successfully!!!")
                        .showInformation();
            }else {
                Notifications.create()
                        .title("Failed")
                        .darkStyle()
                        .position(Pos.CENTER)
                        .hideAfter(Duration.millis(2000))
                        .text("Quiz cannot be saved!! Try again...")
                        .showError();
            }
        }
    }
}
