package sample.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToggleButton;
import com.jfoenix.controls.JFXRadioButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import sample.models.Question;
import sample.models.Quiz;
import sample.models.Student;

import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Pattern;

public class AdminStudentTabController implements Initializable {

    @FXML
    private VBox formContainer;

    @FXML
    private TextField firstName;

    @FXML
    public TextField lastName;

    @FXML
    private TextField mobileNumber;

    @FXML
    private JFXRadioButton male;

    @FXML
    private JFXRadioButton female;

    @FXML
    private JFXButton saveButton;

    @FXML
    private TableView<Student> studentTable;

    @FXML
    private TableColumn<Student, String> studentIdColumn;

    @FXML
    private TableColumn<Student, String> firstNameColumn;

    @FXML
    private TableColumn<Student, String> lastNameColumn;

    @FXML
    private TableColumn<Student, String> mobileNumberColumn;

    @FXML
    private TableColumn<Student, Character> genderColumn;


    @FXML
    private TableColumn<Student, String> emailColumn;

    @FXML
    private TableColumn<Student, String> passwordColumn;

    @FXML
    private TextField email;

    @FXML
    private PasswordField password;

    //NON FXML VARIABLES
    private ToggleGroup toggleGroup;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initAll();
        radioButtonSetup();
        renderTable();
    }



    private void renderTable() {
        List<Student> students = Student.getALl();
        studentTable.getItems().clear();


        this.studentIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        this.lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        this.mobileNumberColumn.setCellValueFactory(new PropertyValueFactory<>("mobile"));
        this.emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        this.passwordColumn.setCellValueFactory(new PropertyValueFactory<>("password"));
        this.genderColumn.setCellValueFactory(new PropertyValueFactory<>("gender"));

        studentTable.getItems().addAll(students);
    }


    private void radioButtonSetup(){
        this.male.setSelected(true);
        this.male.setToggleGroup(this.toggleGroup);
        this.female.setToggleGroup(this.toggleGroup);
    }

    private void initAll(){
        this.toggleGroup = new ToggleGroup();
    }

    private String validate(Student student){
        String errorMessage = null;

        Pattern p = Pattern.compile("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$");

        if(student.getFirstName().length() <= 2){
            errorMessage = "First Name must be more Than 2 Characters Long";
        }else if(student.getLastName().length() <= 2){
            errorMessage = "Last Name must be more Than 2 Characters Long";
        }else if( !(p.matcher(student.getEmail()).matches()) ){
            errorMessage = "Enter Valid Email!!";
        }else if(student.getPassword().length() <= 6){
            errorMessage = "Password must be more Than 6 Characters Long";
        }else if(student.getMobile().length() < 10){
            errorMessage = "Enter valid Mobile Number";
        }

        return errorMessage;
    }

    public void saveStudent(ActionEvent event) {
        System.out.println("Save button clicked.....");
        String firstName = this.firstName.getText().trim();
        String lastName = this.lastName.getText().trim();
        String mobile = this.mobileNumber.getText().trim();
        String email = this.email.getText().trim();
        String password = this.password.getText().trim();
        Character gen = 'M';
        String errorMessage = null;

        JFXRadioButton gender = (JFXRadioButton) toggleGroup.getSelectedToggle();
        if(gender != null){
            if(gender.equals(female)){
                gen = 'F';
            }
        }

        //save student code
        Student s = new Student(firstName, lastName, mobile, email, password, gen);

        //validating student object
        errorMessage = this.validate(s);

        if (errorMessage != null){
            Notifications.create()
                    .title("Fill Student Details Correctly")
                    .text(errorMessage)
                    .position(Pos.CENTER)
                    .darkStyle()
                    .hideAfter(Duration.millis(2000))
                    .showError();
            return;
        }

        if(s.isExists()){
            Notifications.create()
                    .darkStyle()
                    .text("Student Already Exists")
                    .title("Failed!")
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .showError();
            return;
        }

        s = s.save();

        if(s != null){
            Notifications.create()
                    .darkStyle()
                    .text("Student Registered")
                    .title("Success!")
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .showInformation();
            this.resetForm();
            studentTable.getItems().add(0, s);
        }else {
            Notifications.create()
                    .darkStyle()
                    .text("Student Registration Failed")
                    .title("Failed!")
                    .position(Pos.CENTER)
                    .hideAfter(Duration.millis(2000))
                    .showError();
        }

    }


    public void resetForm(){
        this.password.clear();
        this.firstName.clear();
        this.lastName.clear();
        this.email.clear();
        this.mobileNumber.clear();
        this.male.setSelected(true);
    }

}
