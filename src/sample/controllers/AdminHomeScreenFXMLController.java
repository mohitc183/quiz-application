package sample.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AdminHomeScreenFXMLController implements Initializable {

    @FXML
    private TabPane adminTabPane;

    @FXML
    private Tab addQuizTab;

    @FXML
    private Tab addStudentTab;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            Parent root = FXMLLoader.load(getClass().getResource("/sample/fxml/AddQuizFXML.fxml"));
            addQuizTab.setContent(root);

            Parent studentTabNode = FXMLLoader.load(getClass().getResource("/sample/fxml/AdminStudentTab.fxml"));
            addStudentTab.setContent(studentTabNode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
