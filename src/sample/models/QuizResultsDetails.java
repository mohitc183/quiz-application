package sample.models;

import sample.constants.DatabaseConstants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Map;
import java.util.Set;

public class QuizResultsDetails {

    private Integer id;
    private Question question;
    private String userAnswer;
    private QuizResult quizResult;



    public static class MetaData{

        public static final String TABLE_NAME = "QUIZ_RESULT_DETAILS";
        public static final String ID = "ID";
        public static final String USER_ANSWER = "USER_ANSWER";
        public static final String QUESTION_ID = "QUESTION_ID";
        public static final String QUIZ_RESULT_ID = "QUIZ_RESULT_ID";
    }

    public QuizResultsDetails() {
    }

    public QuizResultsDetails(Question question, String userAnswer, QuizResult quizResult) {
        this.question = question;
        this.userAnswer = userAnswer;
        this.quizResult = quizResult;
    }

    public QuizResultsDetails(Integer id, Question question, String userAnswer, QuizResult quizResult) {
        this.id = id;
        this.question = question;
        this.userAnswer = userAnswer;
        this.quizResult = quizResult;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public QuizResult getQuizResult() {
        return quizResult;
    }

    public void setQuizResult(QuizResult quizResult) {
        this.quizResult = quizResult;
    }

    public static void createTable(){


        String raw = "CREATE TABLE %s (" +
                "                %s Integer NOT null PRIMARY KEY AUTOINCREMENT," +
                "                %s INT NOT null," +
                "                %s INT NOT null," +
                "                %s VARCHAAR[200] NOT null," +
                "                FOREIGN KEY (%s) REFERENCES %s(%s)," +
                "                FOREIGN KEY (%s) REFERENCES %s(%s)" +
                ");";

        String query = String.format(raw,
                MetaData.TABLE_NAME,
                MetaData.ID,
                MetaData.QUIZ_RESULT_ID,
                MetaData.QUESTION_ID,
                MetaData.USER_ANSWER,
                MetaData.QUIZ_RESULT_ID,
                QuizResult.MetaData.TABLE_NAME,
                QuizResult.MetaData.ID,
                MetaData.QUESTION_ID,
                Question.MetaData.TABLENAME,
                Question.MetaData.QUESTION_ID
        );

        System.out.println(query);

        try {

            String connectionUrl = DatabaseConstants.CONNECTION_URL;
            Class.forName(DatabaseConstants.DRIVER_CLASS);
            Connection connection = DriverManager.getConnection(connectionUrl);
            PreparedStatement ps = connection.prepareStatement(query);
            boolean b = ps.execute();

            System.out.println("quiz.createTable()");
            System.out.println(b);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean saveQuizResultDetails(QuizResult quizResult, Map<Question, String> userAnswers) {

        String raw = "INSERT  INTO %s( %s, %s, %s) VALUES ( ?, ?, ?);";

        String query = String.format(raw,
                MetaData.TABLE_NAME,
                MetaData.QUIZ_RESULT_ID,
                MetaData.QUESTION_ID,
                MetaData.USER_ANSWER
        );

        System.out.println(query);

        try {

            String connectionUrl = DatabaseConstants.CONNECTION_URL;
            Class.forName(DatabaseConstants.DRIVER_CLASS);
            Connection connection = DriverManager.getConnection(connectionUrl);
            PreparedStatement ps = connection.prepareStatement(query);

            Set<Question> questions = userAnswers.keySet();

            for(Question question : questions){

                ps.setInt(1, quizResult.getId());
                ps.setInt(2, question.getQuestionId());
                ps.setString(3, userAnswers.get(question));

                ps.addBatch();
            }
            int[] result = ps.executeBatch();

            if (result.length > 0){
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

}
