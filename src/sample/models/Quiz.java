package sample.models;

import jdk.jfr.MetadataDefinition;
import sample.constants.DatabaseConstants;

import java.sql.*;
import java.util.*;

public class Quiz {
    private Integer quizId;
    private String title;

    public static class MetaData{

        public static final String TABLENAME = "QUIZZES";
        public static final String TITLE = "TITLE";
        public static final String QUIZ_ID = "ID";
    }

    public Quiz(){

    }

    public Quiz(String title) {
        this.title = title;
    }

    public Integer getQuizId() {
        return quizId;
    }

    public void setQuizId(Integer quizId) {
        this.quizId = quizId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return this.title;
    }

    public static void createTable(){
        try {

            String raw = "CREATE TABLE IF NOT EXISTS %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s VARCHAR[50] );";
            String query = String.format(raw, MetaData.TABLENAME, MetaData.QUIZ_ID, MetaData.TITLE);

            System.out.println(query);
            String connectionUrl = "jdbc:sqlite:quiz.db";
            Class.forName("org.sqlite.JDBC");

            Connection connection = DriverManager.getConnection(connectionUrl);
            PreparedStatement ps = connection.prepareStatement(query);
            boolean b = ps.execute();

            System.out.println(b);
            System.out.println("quiz.createTable()");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int save(){

        String raw = "Insert into %s (%s) values (?)";
        String query = String.format(raw, MetaData.TABLENAME, MetaData.TITLE);
        String connectionUrl = "jdbc:sqlite:quiz.db";

        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (Connection connection = DriverManager.getConnection(connectionUrl);) {

            PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, this.title);

            Integer i = ps.executeUpdate();

            ResultSet keys = ps.getGeneratedKeys();
            if(keys.next()){
                return keys.getInt(1);
            }

            System.out.println(i);
            System.out.println("quiz.save()");

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return -1;
    }

    public boolean save(List<Question> questions){
        boolean flag = true;
        this.quizId = this.save();

        for(Question q : questions){
            flag = flag && q.save();
            System.out.println(flag);
        }

        return flag;
    }

    public static Map<Quiz, List<Question>> getAll(){

        Map<Quiz, List<Question>> quizes = new HashMap<>();
        Quiz key = null;

        String query = String.format("SELECT %s.%s , %s, " +
                        "%s, %s, " +
                        "%s, %s, " +
                        "%s, %s," +
                        " %s" +
                        " FROM %s JOIN %s ON %s.%s = %s.%s; ",
                MetaData.TABLENAME,
                MetaData.QUIZ_ID,
                MetaData.TITLE,
                Question.MetaData.QUESTION_ID,
                Question.MetaData.QUESTION,
                Question.MetaData.OPTION1,
                Question.MetaData.OPTION2,
                Question.MetaData.OPTION3,
                Question.MetaData.OPTION4,
                Question.MetaData.ANSWER,
                MetaData.TABLENAME,
                Question.MetaData.TABLENAME,
                Question.MetaData.TABLENAME,
                Question.MetaData.QUIZ_ID,
                MetaData.TABLENAME,
                MetaData.QUIZ_ID
                );
        String connectionUrl = "jdbc:sqlite:quiz.db";
        System.out.println(query);

        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (Connection connection = DriverManager.getConnection(connectionUrl);) {

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet result = ps.executeQuery();

            while(result.next()){
                Quiz temp = new Quiz();
                temp.setQuizId(result.getInt(1));
                temp.setTitle(result.getString(2));

                Question tempQuestion = new Question();
                tempQuestion.setQuestionId(result.getInt(3));
                tempQuestion.setQuestion(result.getString(4));
                tempQuestion.setOption1(result.getString(5));
                tempQuestion.setOption2(result.getString(6));
                tempQuestion.setOption3(result.getString(7));
                tempQuestion.setOption4(result.getString(8));
                tempQuestion.setAnswer(result.getString(9));

                if(key != null &&key.equals(temp)){
                    quizes.get(key).add(tempQuestion);
                }else {
                    ArrayList<Question> value = new ArrayList<>();
                    value.add(tempQuestion);
                    quizes.put(temp, value);
                }

                key = temp;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return quizes;
    }


    public static Map<Quiz, Integer> getAllWithQuestionCount(){

        Map<Quiz, Integer> quizes = new HashMap<>();
        Quiz key = null;

        String query = String.format("SELECT %s.%s , %s, " +
                        "COUNT (*) AS question_count " +
                        " FROM %s JOIN %s ON %s.%s = %s.%s GROUP BY %s.%s; ",
                MetaData.TABLENAME,
                MetaData.QUIZ_ID,
                MetaData.TITLE,
                MetaData.TABLENAME,
                Question.MetaData.TABLENAME,
                Question.MetaData.TABLENAME,
                Question.MetaData.QUIZ_ID,
                MetaData.TABLENAME,
                MetaData.QUIZ_ID,
                MetaData.TABLENAME,
                MetaData.QUIZ_ID
        );

        String connectionUrl = DatabaseConstants.CONNECTION_URL;
        System.out.println(query);

        try {
            Class.forName(DatabaseConstants.DRIVER_CLASS);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (Connection connection = DriverManager.getConnection(connectionUrl);) {

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet result = ps.executeQuery();

            while(result.next()){
                Quiz temp = new Quiz();
                temp.setQuizId(result.getInt(1));
                temp.setTitle(result.getString(2));
                int count = result.getInt(3);
                quizes.put(temp, count);

            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return quizes;
    }

    //get ques using quiz
    public List<Question> getQuestions(){

        List<Question> quizzes = new ArrayList<>();

        String query = String.format("SELECT %s, %s, " +
                        "%s, %s, " +
                        "%s, %s," +
                        " %s" +
                        " FROM %s where %s = ?; ",
                Question.MetaData.QUESTION_ID,
                Question.MetaData.QUESTION,
                Question.MetaData.OPTION1,
                Question.MetaData.OPTION2,
                Question.MetaData.OPTION3,
                Question.MetaData.OPTION4,
                Question.MetaData.ANSWER,

                Question.MetaData.TABLENAME,
                Question.MetaData.QUIZ_ID
        );
            String connectionUrl = "jdbc:sqlite:quiz.db";
        System.out.println(query);

        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (Connection connection = DriverManager.getConnection(connectionUrl);) {

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, this.quizId);
            ResultSet result = ps.executeQuery();

            while(result.next()){

                Question tempQuestion = new Question();
                tempQuestion.setQuestionId(result.getInt(1));
                tempQuestion.setQuestion(result.getString(2));
                tempQuestion.setOption1(result.getString(3));
                tempQuestion.setOption2(result.getString(4));
                tempQuestion.setOption3(result.getString(5));
                tempQuestion.setOption4(result.getString(6));
                tempQuestion.setAnswer(result.getString(7));
                tempQuestion.setQuiz(this);

                quizzes.add(tempQuestion);

            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return quizzes;
    }


    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;

        if( !(obj instanceof Quiz) )
            return false;

        if(this.quizId == ((Quiz) obj).quizId)
            return true;

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(quizId, title);
    }
}
