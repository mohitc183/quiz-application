package sample.models;


import sample.constants.DatabaseConstants;

import java.sql.*;
import java.util.Date;
import java.util.Map;

public class QuizResult {
    private Integer id;
    private Quiz quiz;
    private Student student;
    private Integer rightAnswers;
    private Timestamp timestamp;

    public static class MetaData{

        public static final String TABLE_NAME = "QUIZ_RESULTS";
        public static final String ID = "id";
        public static final String QUIZ_ID = "QUIZ_ID";
        public static final String STUDENT_ID = "STUDENT_ID";
        public static final String RIGHT_ANSWERS = "RIGHT_ANSWERS";
        public static final String TIMESTAMP = "date_time";
    }


    {
        timestamp = new Timestamp(new Date().getTime());
    }

    public QuizResult() {

    }

    public QuizResult(Quiz quiz, Student student, Integer rightAnswers) {
        this.quiz = quiz;
        this.student = student;
        this.rightAnswers = rightAnswers;
    }

    public QuizResult(Integer id, Quiz quiz, Student student, Integer rightAnswers) {
        this.id = id;
        this.quiz = quiz;
        this.student = student;
        this.rightAnswers = rightAnswers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Integer getRightAnswers() {
        return rightAnswers;
    }

    public void setRightAnswers(Integer rightAnswers) {
        this.rightAnswers = rightAnswers;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public static void createTable(){


        String raw = "CREATE TABLE %s (" +
                "%s Integer NOT null PRIMARY KEY AUTOINCREMENT ," +
                "%s INT NOT null," +
                "%s INT NOT null," +
                "%s INT NOT null," +
                "%s TIMESTAMP NOT null," +
                "FOREIGN KEY (%s) REFERENCES %s(%s)," +
                "FOREIGN KEY (%s) REFERENCES %s(%s)" +
                ")";

        String query = String.format(raw,
                MetaData.TABLE_NAME,
                MetaData.ID,
                MetaData.STUDENT_ID,
                MetaData.QUIZ_ID,
                MetaData.RIGHT_ANSWERS,
                MetaData.TIMESTAMP,
                MetaData.STUDENT_ID,
                Quiz.MetaData.TABLENAME,
                Quiz.MetaData.QUIZ_ID,
                MetaData.STUDENT_ID,
                Student.Metadata.TABLE_NAME,
                Student.Metadata.ID
        );

        System.out.println(query);

        try {

//            String connectionUrl = "jdbc:sqlite:quiz.db";
//            Class.forName("org.sqlite.JDBC");
            String connectionUrl = DatabaseConstants.CONNECTION_URL;
            Class.forName(DatabaseConstants.DRIVER_CLASS);
            Connection connection = DriverManager.getConnection(connectionUrl);
            PreparedStatement ps = connection.prepareStatement(query);
            boolean b = ps.execute();

            System.out.println("quiz.createTable()");
            System.out.println(b);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean save(Map<Question, String> userAnswers){
        String raw = "INSERT INTO %s(%s ,%s ,%s ,%s ) VALUES " +
                "(?,?,?,CURRENT_TIMESTAMP)";

        String query = String.format(raw,
                MetaData.TABLE_NAME,
                MetaData.STUDENT_ID,
                MetaData.QUIZ_ID,
                MetaData.RIGHT_ANSWERS,
                MetaData.TIMESTAMP
        );

        System.out.println(query);

        try {

            String connectionUrl = DatabaseConstants.CONNECTION_URL;
            Class.forName(DatabaseConstants.DRIVER_CLASS);
            Connection connection = DriverManager.getConnection(connectionUrl);
            PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1,  this.getStudent().getId());
            ps.setInt(2,  this.getQuiz().getQuizId());
            ps.setInt(3,  this.getRightAnswers());

            int result = ps.executeUpdate();

            if(result > 0){
                ResultSet keys = ps.getGeneratedKeys();
                if (keys.next()){
                    this.setId(keys.getInt(1));

                    //we will save details
                    return saveQuizResultDetails(userAnswers);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    private boolean saveQuizResultDetails(Map<Question, String> userAnswers){

        return QuizResultsDetails.saveQuizResultDetails(this, userAnswers);
    }

}
