package sample.models;

import sample.constants.DatabaseConstants;
import sample.controllers.LoginController;
import sample.exceptions.LoginException;

import java.sql.*;
import java.util.ArrayList;

public class Student {

    private Integer id;
    private String firstName;
    private String lastName;
    private String mobile;
    private Character gender;
    private String email;
    private String password;


    public Student() {

    }

    public Student(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public static class Metadata {
        public static final String TABLE_NAME = "students";
        public static final String ID = "ID";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String MOBILE = "mobile";
        public static final String GENDER = "gender";
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
    }

    public Student(Integer id, String firstName, String lastName, String mobile, String email, String password,
                   Character gender) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobile = mobile;
        this.gender = gender;
        this.email = email;
        this.password = password;
    }

    public Student(String firstName, String lastName, String mobile, String email, String password, Character gender) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.mobile = mobile;
        this.gender = gender;
        this.email = email;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", mobile='" + mobile + '\'' +
                ", gender=" + gender +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    //create tabel
    public static void createTable() {
        String raw = "CREATE TABLE IF NOT EXISTS %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s VARCHAR" +
                "(20), %s VARCHAR(20), %s VARCHAR(20), %s VARCHAR(20), %s VARCHAR(20), %s char)";
        String query = String.format(raw, Metadata.TABLE_NAME, Metadata.ID, Metadata.FIRST_NAME, Metadata.LAST_NAME,
                Metadata.MOBILE, Metadata.EMAIL, Metadata.PASSWORD, Metadata.GENDER);

        System.out.println(query);

        try {

            String connectionUrl = DatabaseConstants.CONNECTION_URL;
            Class.forName(DatabaseConstants.DRIVER_CLASS);
            Connection connection = DriverManager.getConnection(connectionUrl);
            PreparedStatement ps = connection.prepareStatement(query);
            boolean b = ps.execute();

            System.out.println(b);
            System.out.println("student.createTable()");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //SAVE STUDENT
    public Student save(){
        String raw = "INSERT INTO students (%s , %s , %s , %s , %s , %s) VALUES( " +
                "? , ? , ? , ? , ? , ? );";
        String query = String.format(raw, Metadata.FIRST_NAME, Metadata.LAST_NAME, Metadata.MOBILE, Metadata.EMAIL,
                Metadata.PASSWORD, Metadata.GENDER);
        String connectionUrl = "jdbc:sqlite:quiz.db";

        try {

            Class.forName("org.sqlite.JDBC");
            try (Connection connection = DriverManager.getConnection(connectionUrl);) {

                PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, this.firstName);
                ps.setString(2, this.lastName);
                ps.setString(3, this.mobile);
                ps.setString(4, this.email);
                ps.setString(5, this.password);
                ps.setString(6, String.valueOf(this.gender));
//                ps.setString(6, this.gender.toString());
                int i = ps.executeUpdate();
                ResultSet keys = ps.getGeneratedKeys();
                if(keys.next()){
                    this.id = keys.getInt(1);
                }

                System.out.println("Updated rows : " + i);

                return this;

            } catch (Exception e) {
                e.printStackTrace();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public boolean isExists(){

        String query = String.format("Select * from %s where %s = ?;", Metadata.TABLE_NAME, Metadata.EMAIL);

        String connectionUrl = "jdbc:sqlite:quiz.db";

        try {

            Class.forName("org.sqlite.JDBC");
            try (Connection connection = DriverManager.getConnection(connectionUrl);) {

                PreparedStatement ps = connection.prepareStatement(query);
                ps.setString(1, this.email);

                System.out.println(query);

                ResultSet result = ps.executeQuery();

                if(result.next()){
                    return true;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public void login() throws SQLException, ClassNotFoundException, LoginException {

        String query = String.format("Select %s, %s, %s, %s, %s from %s where %s = ? and %s = ?;",
                Metadata.ID,
                Metadata.FIRST_NAME,
                Metadata.LAST_NAME,
                Metadata.MOBILE,
                Metadata.GENDER,
                Metadata.TABLE_NAME,
                Metadata.EMAIL,
                Metadata.PASSWORD
        );

        System.out.println(query);

        String connectionUrl = "jdbc:sqlite:quiz.db";

        Class.forName("org.sqlite.JDBC");
        try (Connection connection = DriverManager.getConnection(connectionUrl);) {

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, this.email);
            ps.setString(2, this.password);

            ResultSet result = ps.executeQuery();

            if(result.next()){
                this.setId(result.getInt(1));
                this.setFirstName(result.getString(2));
                this.setLastName(result.getString(3));
                this.setMobile(result.getString(4));
                this.setGender(result.getString(5).charAt(0));
            }else {
                throw new LoginException("Login Failed..");
            }
        }
    }

    public static ArrayList<Student> getALl(){
        ArrayList<Student> students = new ArrayList<>();

        String query = String.format("Select %s , %s , %s , %s , %s , %s , %s from %s ;",
                Metadata.ID,
                Metadata.FIRST_NAME,
                Metadata.LAST_NAME,
                Metadata.MOBILE,
                Metadata.EMAIL,
                Metadata.PASSWORD,
                Metadata.GENDER,
                Metadata.TABLE_NAME
        );

        String connectionUrl = "jdbc:sqlite:quiz.db";

        try {

            Class.forName("org.sqlite.JDBC");
            try (Connection connection = DriverManager.getConnection(connectionUrl);) {

                PreparedStatement ps = connection.prepareStatement(query);


                System.out.println(query);

                ResultSet result = ps.executeQuery();

                while(result.next()){

                    Student s = new Student();
                    s.setId(result.getInt(1));
                    s.setFirstName(result.getString(2));
                    s.setLastName(result.getString(3));
                    s.setMobile(result.getString(4));
                    s.setEmail(result.getString(5));
                    s.setPassword(result.getString(6));
                    s.setGender(result.getString(7).charAt(0));

                    students.add(s);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return students;
    }
}
